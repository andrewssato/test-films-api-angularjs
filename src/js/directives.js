
app.directive("errormessage", function($rootScope) {
  return {
    restrict: "E",
    link: function(scope, element) {
      scope.errormessageEl = element;
      scope.errormessageEl[0].innerText = "";

      $rootScope.$on('error:search', function(evt, data) {
        this.errormessageEl[0].innerText = data.Error || ""
      }.bind(scope));
    }
  }
});

app.directive("loading", function($rootScope) {
  return {
    restrict: "E",
    link: function(scope, element) {
      scope.loadingEl = element;

      $rootScope.$on('active:search', function(evt, data) {
        this.loadingEl.removeClass('ng-hide');
      }.bind(scope));

      $rootScope.$on('success:data', function(evt, data) {
        this.loadingEl.addClass('ng-hide');
      }.bind(scope));

      $rootScope.$on('error:data', function(evt, data) {
        this.loadingEl.addClass('ng-hide');
      }.bind(scope));
    }
  }
});

app.directive("loadmore", function($rootScope) {
  return {
    restrict: "E",
    link: function(scope, element) {
      scope.loadmoreEl = element;

      $rootScope.$on('success:data', function(evt, data) {
        this.loadmoreEl.removeClass('ng-hide');
      }.bind(scope));

      $rootScope.$on('error:data', function(evt, data) {
        this.loadmoreEl.addClass('ng-hide');
      }.bind(scope));
    }
  }
});

app.directive("tooltippreview", function($rootScope) {
  return {
    restrict: "A",
    link: function(scope, element) {
      scope.tooltipPreviewEl = element;

      element.bind("mouseover", function() {
        this.tooltipPreviewEl.removeClass('tooltip-hide');  
      }.bind(scope));

      element.bind("mouseout", function() {
        this.tooltipPreviewEl.addClass('tooltip-hide');  
      }.bind(scope));
    }
  }
});

app.directive("imageloader", function($rootScope) {
  return {
    restrict: "A",
    link: function(scope, element, attrs) {
      element[0].onerror = function () { 
        element[0].src = "http://www.cellshop.com.py/imgs/i/210x210-1-c-80-0-0-0/r/img/no-img.jpg"
      };
    }
  }
});

app.directive("formsearch", function($rootScope) {
  return {
    restrict: "A",
    link: function(scope, element, attrs) {
      $rootScope.formWrapEl = element;
    }
  }
});
