app.controller("DetailsCtrl", ['$scope', '$http', '$stateParams',
  function($scope, $http, $stateParams) {

    $scope.filmId = $stateParams.filmId;
    $scope.searchValue = $stateParams.searchValue;

    $scope.dataDetails = {}
    $scope.itemKeys = [];

    $http
      .get('http://www.omdbapi.com', {
        params: {
          i: $scope.filmId
        }
      })
      .success(function(data, status) {
        if (data.Response === "True") {
          $scope.successData(data, status);
        } else {
          $scope.errorData(data, status);
        }
        $scope.$emit('hide:loading');
      })

    $scope.successData = function(data) {
      $scope.dataDetails = data;    
      $scope.itemKeys = window.Object.keys(data);
      $scope.removeKeys(['Poster', 'Title']);
      $scope.$emit('success:data', data);
    };

    $scope.errorData = function(data) {
      $scope.$emit('error:search', data);
    };

    $scope.removeKeys = function(keys) {
      keys.forEach(function(i){
        $scope.itemKeys.splice($scope.itemKeys.indexOf(i), 1);
      })
    }
  }
]);