app.controller("ListCtrl", ['$scope', '$http', '$stateParams',
  function($scope, $http, $stateParams) {

    $scope.searchValue = $stateParams.searchValue;

    if ($stateParams.searchValue) {
      $scope.search($scope.searchValue);
    }

    $scope.resetSearch = function() {
      $scope.itemPerPage = 6;
      $scope.currentPage = 0;
      $scope.results = [];
      $scope.currentList = [];
    };

    $scope.getCurrentItems = function() {
      var totalRendered  = $scope.currentPage * $scope.itemPerPage,
          limitNextItems = totalRendered + $scope.itemPerPage;

      $scope.currentList = $scope.currentList.concat($scope.results.slice(totalRendered, limitNextItems));
    };

    $scope.request = function(name) {
      $http
        .get('http://www.omdbapi.com', {
          params: {
            page: $scope.currentPage+1,
            s: name
          }
        })
        .success(function(data, status) {
          if (data.Response === "True") {
            $scope.successData(data, status);
          } else {
            $scope.errorData(data, status);
          }
        })
    };   

    $scope.successData = function(data) {
      $scope.results = $scope.results.concat(data.Search);
      $scope.totalResults = $scope.results.length;   
      $scope.getCurrentItems();
      $scope.$emit('success:data', data);
      $scope.formWrapEl.addClass('searched');
    };  

    $scope.errorData = function(data) {
      $scope.$emit('error:search', data);
      $scope.$emit('error:data');
    };

    $scope.search = function(name) {
      if (!name) {
        return false;
      }

      $scope.$emit('active:search');
      $scope.searchValue = name;
      $scope.resetSearch();
      $scope.request(name);
    };

    $scope.loadMore = function() {
      $scope.currentPage++
      
      if ($scope.results.length - $scope.currentList.length >= $scope.itemPerPage) {
        $scope.getCurrentItems();
      } else {
        $scope.request($scope.searchValue);
      }
    };
  }
]);

