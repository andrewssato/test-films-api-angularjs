var app = angular.module("FilmsApp", ['ui.router', 'ngMessages', 'ngMaterial']);

app.config(function config($stateProvider) {
  $stateProvider
    .state("index", {
      url: "",
      controller: "ListCtrl",
      templateUrl: "templates/home.html"
    })

    .state("index.search", {
      url: "/:searchValue",
      controller: "ListCtrl",
      templateUrl: "templates/home.html"
    })

    .state("details", {
      url: "/details/:filmId/:searchValue",
      controller: "DetailsCtrl",
      templateUrl: "templates/details.html"
    })

})
